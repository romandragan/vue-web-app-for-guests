require('dotenv').config();

const axios = require('axios');
const AWS = require('aws-sdk');
const admin = require('firebase-admin');
const functions = require('firebase-functions');
const path = require('path');
const rp = require('request-promise');
const sgMail = require('@sendgrid/mail');
const streamingS3 = require('streaming-s3');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

const serviceAccount = require('./adminsdk-credentials.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: process.env.FIREBASE_DB_URL,
});

exports.sendToAmazon = functions.runWith({
  timeoutSeconds: 300,
  memory: '1GB',
}).storage.object().onFinalize((object) => {
  const filePath = object.name;
  const contentType = object.contentType;

  if (!contentType.startsWith('video/')
      || !filePath.startsWith(`${process.env.FIREBASE_STORAGE_VIDEOS_DIR}/`)
  ) {
    return null
  }

  const fileName = path.basename(filePath);
  const file = admin.storage().bucket(object.bucket).file(filePath);
  const transactionUid = path
    .basename(fileName, process.env.FIREBASE_STORAGE_VIDEO_EXTENSION)
    .replace(process.env.FIREBASE_STORAGE_VIDEO_POSTFIX, '');

  let order;
  let inputBucketPath;

  AWS.config.update({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: process.env.AWS_REGION,
  });

  return getOrder(transactionUid).then((fetchedOrder) => {
    order = fetchedOrder;
    inputBucketPath = `uploads/${order.creatorId}/${fileName}`;

    return;
  }).then(() => new Promise((resolve, reject) => new streamingS3(
    file.createReadStream(),
    { accessKeyId: process.env.AWS_ACCESS_KEY, secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY },
    {
      Bucket: process.env.AWS_INPUT_BUCKET_NAME,
      Key: inputBucketPath,
      ContentType: contentType,
    },
    (e, resp, stats) => {
      if (e) {
        console.log('Upload error: ', e);
        reject(e);

        return;
      }

      console.log('Upload successful: ', resp);

      resolve(resp);
    }
  ))).then(() => {
    const elastictranscoder = new AWS.ElasticTranscoder();

    const params = {
      PipelineId: process.env.AWS_PIPELINE_ID,
      Input: {
        Key: inputBucketPath,
      },
      OutputKeyPrefix: `${order.creatorId}/`,
      Output: {
        Key: fileName,
        PresetId: process.env.AWS_PRESET_ID,
        Watermarks: order.public
            ? [{
              InputKey: process.env.AWS_WATERMARK_KEY,
              PresetWatermarkId: process.env.AWS_WATERMARK_POSITION,
            }]
            : undefined,
      },
    };

    return new Promise((resolve, reject) => elastictranscoder.createJob(params, (err, data) => {
      if (err) {
        console.log('Transcoding "createJob" error', err, err.stack);
        reject(err);

        return;
      }

      console.log('Transcoding "createJiob" success', data);

      elastictranscoder.waitFor(
        'jobComplete',
        { Id: data.Job.Id },
        (err, jobCompleteData) => {
          if (err) {
            console.log('Transcoding "waitFor" error', err, err.stack);
            reject(err);

            return;
          }

          resolve(jobCompleteData);
      });
    }));
  }).then((data) => {
    console.log('Transcoding complete', data);

    const s3 = new AWS.S3()
    const key = `${data.Job.OutputKeyPrefix}${data.Job.Output.Key}`;
    const params = {
      Bucket: process.env.AWS_OUTPUT_BUCKET_NAME,
      Key: key,
      ACL: 'public-read',
    };

    return new Promise((resolve, reject) => {
      s3.putObjectAcl(params, (err) => {
        if (err) {
          return reject(err);
        }

        resolve(key);
      });
    });
  }).then((key) => {
    const downloadURL = `https://${process.env.AWS_OUTPUT_BUCKET_NAME}.s3.amazonaws.com/${key}`;
    const transactionsRef = admin.database().ref('transactions');

    return Promise.all([
      transactionsRef.child(transactionUid).update({
        status: 'completed',
        downloadURL,
      }),
      sendEmail(order, downloadURL),
      captureCharge(order.transactionId),
    ]);
  });
});

exports.createCharge = functions.https.onCall((data, context) => {
  let newCharge = null

  return stripe.charges.create({
    amount: data.orderAmount,
    currency: 'usd',
    source: data.token.id,
    capture: false,
  }).then((charge) => {
    newCharge = charge;

    return getFan(context.auth.uid);
  }).then((fan) => {

    const dueTimeStamp = newCharge.created + (3600 * 24 * 7); // 7 days
    const transactionsRef = admin.database().ref('transactions');
    const newTransaction = transactionsRef.push();

    return newTransaction.set({
      creatorId: data.creatorId,
      creatorName: data.creatorName,
      dueTimeStamp,
      fanId: context.auth.uid,
      price: newCharge.amount / 100,
      public: !data.order.isPrivate,
      question1: data.order.forElseName,
      question2: data.order.notes,
      question3: fan ? `${fan.bio.firstName} ${fan.bio.lastName}` : data.order.guest.name,
      rating: data.rating,
      secret_index: '', // ?
      status: 'pending',
      transactionEndTimeStamp: dueTimeStamp,
      transactionId: newCharge.id,
      transactionStartTimeStamp: newCharge.created,
      videoURL: '',
      gift: data.order.for === 'else',
      email: data.token.email,
    })
  })
  .then(() => newCharge.status)
  .catch((error) => error);
});

exports.remoteConfig = functions.https.onCall((data, context) => {
  return admin.credential.applicationDefault().getAccessToken()
    .then(({ access_token }) => rp({
      uri: `https://firebaseremoteconfig.googleapis.com/v1/projects/${process.env.GCP_PROJECT}/remoteConfig`, // eslint-disable-line
      headers: {
        Authorization: 'Bearer ' + access_token
      },
      json: true,
    }))
    .then((config) => config.parameters)
    .catch((error) => error);
});

exports.branchLink = functions.database.ref('/creators/{userId}/bio/isVerified')
  .onUpdate((snapshot, context) => {
    const isVerified = snapshot.after.val();

    if (!isVerified) {
      return Promise.resolve();
    }

    return getCreator(context.params.userId).then((creator) => {
      if (!creator.bio.instagramUsername || creator.bio.branch_url) {
        return;
      }

      const desktopUrl = `${process.env.APP_ORIGIN}/${creator.bio.instagramUsername}`

      return axios.post('https://api.branch.io/v1/url', {
        branch_key: process.env.BRANCH_API_KEY,
        channel: 'app',
        feature: 'share profile',
        campaign: 'share',
        alias: creator.bio.instagramUsername,
        data: {
          $canonical_identifier: `creator/${context.params.userId}`,
          $og_title: 'Find me on Storybit!',
          $og_description: 'Order your personalized video shoutout',
          $desktop_url: desktopUrl,
          $canonical_url: desktopUrl,
          $web_only: true,
          $ios_app: 2,
          $android_app: 2,
          $android_url: desktopUrl,
          $ios_url: desktopUrl,
        },
      })
      .then((response) => {
        return snapshot.after.ref.parent.child('branch_url').set(response.data.url);
      })
      .catch((error) => {
        console.log('error', error);
        throw error;
      });
    });
  });

function captureCharge(chargeId) {
  return new Promise((resolve, reject) => stripe.charges.capture(
    chargeId,
    (err, charge) => {
      if (err) {
        console.log('Carge capture error', err);
        reject(err);

        return;
      }

      resolve(charge);
    }
  ));
}

function getOrder(uid) {
  return admin.database()
    .ref(`transactions/${uid}`)
    .once('value')
    .then(snapshot => snapshot.val())
}

function getFan(uid) {
  return admin.database()
    .ref(`fans/${uid}`)
    .once('value')
    .then(snapshot => snapshot.val())
}

function getCreator(uid) {
  return admin.database()
    .ref(`creators/${uid}`)
    .once('value')
    .then(snapshot => snapshot.val())
}

function sendEmail(order, downloadURL) {
  sgMail.setApiKey(process.env.SENDGRID_API_KEY);

  return getFan(order.fanId).then((fan) => {
    if (fan) {
      return `${fan.bio.firstName} ${fan.bio.lastName}`;
    }

    return order.question3;
  }).then((fullname) => sgMail.send({
    to: order.email,
    from: process.env.SENDGRID_FROM,
    templateId: process.env.SENDGRID_TEMPLATE_ID,
    dynamic_template_data: {
      downloadURL,
      fullname,
    },
  }));
}
