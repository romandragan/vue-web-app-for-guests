import axios from 'axios';
import Vue from 'vue';
import * as VueFire from 'vuefire';
import VueAuthenticate from 'vue-authenticate';
import VueAxios from 'vue-axios';
import VueStripeCheckout from 'vue-stripe-checkout';

import App from './App.vue';
import router from './router';
import store from './store';
import Firebase from './vendors/Firebase';

require('dotenv').config();

Vue.config.productionTip = false;

Vue.use(VueFire);

Vue.use(VueAxios, axios);

Vue.use(VueAuthenticate, {
  baseUrl: window.location.origin,
  providers: {
    instagram: {
      clientId: process.env.INSTAGRAM_CLIENT_ID,
      responseType: 'token',
      name: 'instagram',
      url: '/verify-profile',
      authorizationEndpoint: 'https://api.instagram.com/oauth/authorize',
      redirectUri: `${window.location.origin}/verify-profile`,
      requiredUrlParams: ['scope'],
      scope: ['basic'],
      scopeDelimiter: '+',
      oauthType: '2.0',
      popupOptions: { width: 500, height: 500 },
    },
  },
});

Firebase.init();

Vue.use(VueStripeCheckout, process.env.STRIPE_PUBLISHABLE_KEY);

Vue.filter('remoteConfig', (value) => {
  const setting = store.getters.remoteConfig[value];

  return setting ? setting.defaultValue.value : '';
});

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
