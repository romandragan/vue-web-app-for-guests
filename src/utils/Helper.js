import Firebase from '@/vendors/Firebase';

export default {
  getUser(uid) {
    return Firebase.instance.database()
      .ref(`users/${uid}`)
      .once('value')
      .then(snapshot => snapshot.val());
  },
  getFan(uid) {
    return Firebase.instance.database()
      .ref(`fans/${uid}`)
      .once('value')
      .then(snapshot => snapshot.val());
  },
  getCreator(id) { // uid / instagramUsername
    if (id.includes('.')
      || id.includes('#')
      || id.includes('$')
      || id.includes('[')
      || id.includes(']')
    ) {
      return this.getCreatorByInstagram(id);
    }

    return Firebase.instance.database()
      .ref(`creators/${id}`)
      .once('value')
      .then(snapshot => snapshot.val())
      .then((creator) => {
        if (creator) {
          creator.uid = id;
          return creator;
        }

        return this.getCreatorByInstagram(id);
      });
  },
  getCreatorByInstagram(instagramUsername) {
    return Firebase.instance.database()
      .ref('creators')
      .orderByChild('bio/instagramUsername')
      .equalTo(`${instagramUsername}`)
      .limitToFirst(1)
      .once('value')
      .then(snapshot => snapshot.val())
      .then((result) => {
        if (!result) {
          return null;
        }

        const uid = Object.keys(result)[0];
        return {
          uid,
          ...result[uid],
        };
      });
  },
  getRemoteProperty(remoteConfig, name) {
    if (!remoteConfig[name]) {
      return '';
    }

    return remoteConfig[name].defaultValue.value;
  },
  branchLink(instagramUsername, uid) {
    // eslint-disable-next-line
    (function(b,r,a,n,c,h,_,s,d,k){if(!b[n]||!b[n]._q){for(;s<_.length;)c(h,_[s++]);d=r.createElement(a);d.async=1;d.src="https://cdn.branch.io/branch-latest.min.js";k=r.getElementsByTagName(a)[0];k.parentNode.insertBefore(d,k);b[n]=h}})(window,document,"script","branch",function(b,r){b[r]=function(){b._q.push([r,arguments])}},{_q:[],_v:1},"addListener applyCode autoAppIndex banner closeBanner closeJourney creditHistory credits data deepview deepviewCta first getCode init link logout redeem referrals removeListener sendSMS setBranchViewData setIdentity track validateCode trackCommerceEvent logEvent disableTracking".split(" "), 0);

    const desktopUrl = `${document.location.origin}/${instagramUsername}`;
    const linkData = {
      channel: 'app',
      feature: 'share profile',
      campaign: 'share',
      alias: instagramUsername,
      data: {
        $canonical_identifier: `creator/${uid}`,
        $og_title: 'Find me on Storybit!',
        $og_description: 'Order your personalized video shoutout',
        $desktop_url: desktopUrl,
        $canonical_url: desktopUrl,
        $web_only: true,
        $ios_app: 2,
        $android_app: 2,
        $android_url: desktopUrl,
        $ios_url: desktopUrl,
      },
    };
    branch.init(process.env.BRANCH_API_KEY);
    branch.link(linkData, (err, data) => {
      if (err) {
        console.log(err);
      } else {
        Firebase.instance.database().ref('creators').child(`${uid}/bio`).update({
          branch_url: data,
        });
      }
    });
  },
};
