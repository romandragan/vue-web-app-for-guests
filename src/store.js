import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

import Firebase from '@/vendors/Firebase';

Vue.use(Vuex);

export default new Vuex.Store({
  plugins: [createPersistedState({
    paths: [
      'remoteConfig',
    ],
  })],
  state: {
    authorized: false,
    remoteConfig: null,
    justLoggedIn: false,
  },
  getters: {
    authorized: state => state.authorized,
    remoteConfig: state => state.remoteConfig || {},
    justLoggedIn: state => state.justLoggedIn,
  },
  mutations: {
    setAuthorized(state, payload) {
      state.authorized = payload;
    },
    setRemoteConfig(state, payload) {
      state.remoteConfig = payload;
    },
    setJustLoggedIn(state, payload) {
      state.justLoggedIn = payload;
    },
  },
  actions: {
    fetchRemoteConfig({ commit }) {
      const remoteConfig = Firebase.instance.functions().httpsCallable('remoteConfig');
      remoteConfig().then(({ data }) => {
        commit('setRemoteConfig', data);
      });
    },
  },
});
