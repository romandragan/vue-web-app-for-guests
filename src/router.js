import Vue from 'vue';
import Router from 'vue-router';

import UserType from '@/models/UserType';
import store from '@/store';
import Helper from '@/utils/Helper';
import Firebase from '@/vendors/Firebase';

Vue.use(Router);

function onAuthStateChanged(callback) {
  Firebase.instance.auth().onAuthStateChanged((user) => {
    callback(user);
  });
}

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      beforeEnter: (to, from, next) => {
        onAuthStateChanged((user) => {
          if (user && !user.isAnonymous) {
            Helper.getUser(user.uid).then(({ userType }) => {
              if (userType === UserType.FAN) {
                next('/my-orders');
              } else if (userType === UserType.CREATOR) {
                next({ name: 'creator-profile', params: { id: user.uid } });
              }
            });
          } else {
            next('/login');
          }
        });
      },
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "my-orders" */ './views/Login.vue'),
      meta: { onlyGuest: true },
    },
    {
      path: '/sign-up',
      name: 'sign-up',
      component: () => import(/* webpackChunkName: "sign-up" */ './views/sign-up/Intro.vue'),
      meta: { onlyGuest: true },
    },
    {
      path: '/sign-up/:role',
      name: 'sign-up-form',
      props: true,
      component: () => import(/* webpackChunkName: "sign-up-form" */ './views/sign-up/Form.vue'),
      meta: { onlyGuest: true },
    },
    {
      path: '/verify-profile',
      name: 'sign-up-verify-profile',
      component: () => import(/* webpackChunkName: "sign-up-verify-profile" */ './views/sign-up/VerifyProfile.vue'),
      meta: { requiresAuth: true, onlyCreator: true },
    },
    {
      path: '/set-rates',
      name: 'sign-up-set-rates',
      component: () => import(/* webpackChunkName: "sign-up-set-rates" */ './views/sign-up/SetRates.vue'),
      meta: { requiresAuth: true, onlyCreator: true },
    },
    {
      path: '/my-orders',
      name: 'my-orders',
      component: () => import(/* webpackChunkName: "my-orders" */ './views/MyOrders.vue'),
      meta: { requiresAuth: true, onlyFan: true },
    },
    {
      path: '/forgot-password',
      name: 'forgot-password',
      component: () => import(/* webpackChunkName: "forgot-password" */ './views/ForgotPassword.vue'),
    },
    {
      path: '/creator/:id',
      alias: '/:id',
      name: 'creator-profile',
      props: true,
      component: () => import(/* webpackChunkName: "creator-profile" */ './views/CreatorProfile.vue'),
    },
  ],
});

router.beforeEach((to, from, next) => {
  onAuthStateChanged((user) => {
    const isAuthorized = (user !== null && !user.isAnonymous);

    store.commit('setAuthorized', isAuthorized);

    if (to.matched.some(record => record.meta.requiresAuth)) {
      if (isAuthorized) {
        Helper.getUser(user.uid).then(({ userType }) => {
          if (to.meta.onlyFan && userType !== UserType.FAN) {
            next({ name: 'creator-profile', params: { id: user.uid } });
          } else if (to.meta.onlyCreator && userType !== UserType.CREATOR) {
            next('/my-orders');
          }
          next();
        });
      } else {
        next('/login');
      }
    } else if (to.matched.some(record => record.meta.onlyGuest)) {
      if (isAuthorized) {
        next('/');
      } else {
        next();
      }
    } else {
      next();
    }
  });
});

export default router;
